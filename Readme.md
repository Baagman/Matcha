# Matcha
This is a dating site like project, which will mimic some functionalities like Tinder & other dating sites

## Technologies

- [x] Node Js
- [x] Express FrameWork
- [x] MongoDB
- [x] EJS Templating
- [x] MDBootstrap
- [x] HTML5
- [x] CSS3
- [x] JavaScript


## AUTHOR

> Musa Baloyi | Xxtractz